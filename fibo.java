{\rtf1\ansi\ansicpg1251\cocoartf1504\cocoasubrtf840
{\fonttbl\f0\fmodern\fcharset0 Courier-Bold;\f1\fmodern\fcharset0 Courier;\f2\fmodern\fcharset0 Courier-Oblique;
\f3\fnil\fcharset0 Monaco;\f4\fmodern\fcharset0 Courier-BoldOblique;}
{\colortbl;\red255\green255\blue255;\red0\green0\blue109;\red109\green109\blue109;\red82\green0\blue103;
\red15\green112\blue3;\red0\green0\blue254;}
{\*\expandedcolortbl;;\csgenericrgb\c0\c0\c42745;\csgenericrgb\c42745\c42745\c42745;\csgenericrgb\c32157\c0\c40392;
\csgenericrgb\c5882\c43922\c1176;\csgenericrgb\c0\c0\c99608;}
\paperw11900\paperh16840\margl1440\margr1440\vieww26820\viewh12300\viewkind0
\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\pardirnatural\partightenfactor0

\f0\b\fs26 \cf2 import 
\f1\b0 \cf0 java.util.Scanner;\
\

\f0\b \cf2 public class 
\f1\b0 \cf0 RecursivMassiv \{\
    
\f2\i \cf3 /**\
     * 
\f3\i0 \uc0\u1052 \u1077 \u1090 \u1086 \u1076 
\f2\i  main\
     */\
    
\f0\i0\b \cf2 public static void 
\f1\b0 \cf0 main(String[] args) \{\
        Scanner input = 
\f0\b \cf2 new 
\f1\b0 \cf0 Scanner(System.
\f4\i\b \cf4 in
\f1\i0\b0 \cf0 );\
\
        
\f2\i \cf3 // 
\f3\i0 \uc0\u1055 \u1086 \u1083 \u1091 \u1095 \u1080 \u1090 \u1100 
\f2\i  
\f3\i0 \uc0\u1080 \u1085 \u1076 \u1077 \u1082 \u1089 
\f2\i  
\f3\i0 \uc0\u1095 \u1080 \u1089 \u1083 \u1072 
\f2\i  
\f3\i0 \uc0\u1060 \u1080 \u1073 \u1086 \u1085 \u1072 \u1095 \u1095 \u1080 
\f2\i \
        
\f1\i0 \cf0 System.
\f4\i\b \cf4 out
\f1\i0\b0 \cf0 .print(
\f0\b \cf5 "
\f3\b0 \uc0\u1042 \u1074 \u1077 \u1076 \u1080 \u1090 \u1077 
\f0\b  
\f3\b0 \uc0\u1080 \u1085 \u1076 \u1077 \u1082 \u1089 
\f0\b  
\f3\b0 \uc0\u1095 \u1080 \u1089 \u1083 \u1072 
\f0\b  
\f3\b0 \uc0\u1060 \u1080 \u1073 \u1086 \u1085 \u1072 \u1095 \u1095 \u1080 
\f0\b : "
\f1\b0 \cf0 );\
        
\f0\b \cf2 int 
\f1\b0 \cf0 index = input.nextInt();\
\
        
\f2\i \cf3 // 
\f3\i0 \uc0\u1053 \u1072 \u1081 \u1090 \u1080 
\f2\i  
\f3\i0 \uc0\u1080 
\f2\i  
\f3\i0 \uc0\u1086 \u1090 \u1086 \u1073 \u1088 \u1072 \u1079 \u1080 \u1090 \u1100 
\f2\i  
\f3\i0 \uc0\u1095 \u1080 \u1089 \u1083 \u1086 
\f2\i  
\f3\i0 \uc0\u1060 \u1080 \u1073 \u1086 \u1085 \u1072 \u1095 \u1095 \u1080 
\f2\i \
        
\f1\i0 \cf0 System.
\f4\i\b \cf4 out
\f1\i0\b0 \cf0 .println(
\f0\b \cf5 "
\f3\b0 \uc0\u1063 \u1080 \u1089 \u1083 \u1086 
\f0\b  
\f3\b0 \uc0\u1060 \u1080 \u1073 \u1086 \u1085 \u1072 \u1095 \u1095 \u1080 
\f0\b  
\f3\b0 \uc0\u1089 
\f0\b  
\f3\b0 \uc0\u1080 \u1085 \u1076 \u1077 \u1082 \u1089 \u1086 \u1084 
\f0\b  "\
                
\f1\b0 \cf0 + index + 
\f0\b \cf5 " 
\f3\b0 \uc0\u1088 \u1072 \u1074 \u1085 \u1086 
\f0\b  " 
\f1\b0 \cf0 + 
\f2\i fib
\f1\i0 (index));\
    \}\
\
    
\f2\i \cf3 /**\
     * 
\f3\i0 \uc0\u1053 \u1072 \u1093 \u1086 \u1076 \u1080 \u1090 
\f2\i  
\f3\i0 \uc0\u1095 \u1080 \u1089 \u1083 \u1086 
\f2\i  
\f3\i0 \uc0\u1060 \u1080 \u1073 \u1086 \u1085 \u1072 \u1095 \u1095 \u1080 
\f2\i \
     */\
    
\f0\i0\b \cf2 public static int 
\f1\b0 \cf0 fib(
\f0\b \cf2 int 
\f1\b0 \cf0 index) \{\
       
\f0\b \cf2 int 
\f1\b0 \cf0 f0 = \cf6 0\cf0 , f1 = \cf6 1\cf0 , f2 = \cf6 0\cf0 ;\
       
\f0\b \cf2 for 
\f1\b0 \cf0 (
\f0\b \cf2 int 
\f1\b0 \cf0 i = \cf6 2\cf0 ; i <= index; i++)\{\
           f2 = f0 + f1;\
           f0 = f1;\
           f1 = f2;\
       \}\
        
\f0\b \cf2 return 
\f1\b0 \cf0 f2;\
    \}\
\}\
}